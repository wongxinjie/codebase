package loadbalance


type Server struct {
	IP string
	Name string
	Weight int
	Active int
	Health bool
}

type LoadBalance interface {
	Select(ip string) *Server
}

type Cluster struct {
	Servers []*Server
}
func NewCluster() *Cluster{
	return &Cluster{Servers: make([]*Server, 0)}
}

func (c *Cluster) AddServer(server *Server) {
	c.Servers = append(c.Servers, server)
}

func (c *Cluster) EmptyOrOneServer() bool {
	return len(c.Servers) == 0 || len(c.Servers) == 1
}

func (c *Cluster) PickOne() *Server {
	if len(c.Servers) == 0 {
		return nil
	}
	return c.Servers[0]
}
