package main

import (
	"fmt"
	"log"
	"net/rpc"
	"time"

	"gitlab.com/wongxinjie/codebase/rpc/kvdb"
)

func main() {
	client, err := rpc.Dial("tcp", "localhost:9094")
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		var keyChange string
		err := client.Call(fmt.Sprintf("%s.Watch", kvdb.KVStoreServiceName), 30, &keyChange)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("watch: ", keyChange)
	}()

	err = client.Call(fmt.Sprintf("%s.Set", kvdb.KVStoreServiceName), [2]string{"key", "value"}, new(struct{}))
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(2 * time.Second)

	err = client.Call(fmt.Sprintf("%s.Set", kvdb.KVStoreServiceName), [2]string{"key", "value-1"}, new(struct{}))
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(5 * time.Second)
}
