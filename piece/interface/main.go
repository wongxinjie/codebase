package main

import "fmt"

func main() {
	x := 200
	var any interface{} = x
	fmt.Println(any)

	g := Gopher{lan: "Go"}
	var c coder = g
	fmt.Println(c)
}

type coder interface {
	code()
	debug()
}

type Gopher struct {
	lan string
}

func (p Gopher) code() {
	fmt.Printf("code=%s\n", p.lan)
}

func (p Gopher) debug() {
	fmt.Printf("debug code=%s\n", p.lan)
}
