package main

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

const script = `
	local orderNum = ARGV[1]
	local province = ARGV[2]
	local station = ARGV[3]
	
	local productIds = {}
	local stockKeys = {}
	-- 获取传入的商品id
	for k, v in pairs(ARGV) do
		if k > 3 then
			local sk = string.format("s:prod_%d", v)
			stockKeys[v] = sk
			productIds[v] = v
		end
	end
	
	local stationField = string.format("%s:%s", province, station)
	local provinceField = string.format("%s:0", province)
	local nationField = "0:0"
	
	local applyRecords = {}

	local function checkLeftNum(key, field)
		if redis.call("HEXISTS", key, field) == 0 then
			return ""
		end

		local count = redis.call("HGET", key, field)
		if  tonumber(count) > 0 then
			return field
		else
			return ""
		end
	end
	
	local canBuy = true
	for k, v in pairs(stockKeys) do
		local apply = checkLeftNum(v, stationField)
		if string.len(apply) > 0 then
			applyRecords[k] = apply
		else
			local apply = checkLeftNum(v, provinceField)
			if string.len(apply) > 0 then
				applyRecords[k] = apply
			else
				local apply = checkLeftNum(v, nationField)
				if string.len(apply) > 0 then
					applyRecords[k] = apply
				else
					canBuy = false
					break
				end
			end
		end
	end
	
	-- 快速失败
	if canBuy == false then
		return false
	end

	local logKey = string.format("log:%s", orderNum)
	for k, v in pairs(applyRecords) do
		local key = stockKeys[k]
		redis.call("HINCRBY", key, v, -1)
		redis.call("HSET", logKey, k, v)
	end
	
	return true
`

func paidOnStock(orderNum string, provinceId, stationId int, productIds []int64) error {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	keys := []interface{}{orderNum, fmt.Sprintf("%d", provinceId), fmt.Sprintf("%d", stationId)}
	for _, p := range productIds {
		keys = append(keys, fmt.Sprintf("%d", p))
	}

	change := redis.NewScript(script)

	_, err := change.Run(context.Background(), rdb, []string{}, keys...).Result()
	if err != nil {
		return err
	}

	return nil
}

func main() {
	orderNum := fmt.Sprintf("%d", time.Now().UnixNano())

	err := paidOnStock(orderNum, 1024, 2048, []int64{1, 2, 3})
	if err != nil {
		fmt.Println(err)
	}
}
