package main

import (
	"log"
	"net"
	"net/rpc"

	"gitlab.com/wongxinjie/codebase/rpc/kvdb"
)

func main() {
	err := rpc.RegisterName(kvdb.KVStoreServiceName, kvdb.NewKVStoreService())
	if err != nil {
		log.Fatal(err)
	}

	srv, err := net.Listen("tcp", ":9094")
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := srv.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go rpc.ServeConn(conn)
	}
}
