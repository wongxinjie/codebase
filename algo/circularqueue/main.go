package main

import (
	"errors"
	"fmt"
)

type CircularQueue struct {
	front    int
	rear     int
	used     int
	capacity int
	items    []int
}

func New(c int) *CircularQueue {
	return &CircularQueue{capacity: c, items: make([]int, c)}
}

func (q *CircularQueue) Enqueue(v int) error {
	if q.IsFull() {
		return errors.New("CircularQueue full")
	}

	q.items[q.rear] = v
	q.rear = (q.rear + 1) % q.capacity
	q.used++
	return nil
}

func (q *CircularQueue) Dequeue() error {
	if q.IsEmpty() {
		return errors.New("CircularQueue empty")
	}
	q.front = (q.front + 1) % q.capacity
	q.used--
	return nil
}

func (q *CircularQueue) Front() int {
	if q.IsEmpty() {
		return 0
	}
	return q.items[q.front]
}

func (q *CircularQueue) Rear() int {
	if q.IsEmpty() {
		return 0
	}
	tail := (q.rear - 1 + q.capacity) % q.capacity
	return q.items[tail]
}

func (q *CircularQueue) IsEmpty() bool {
	return q.used == 0
}

func (q *CircularQueue) IsFull() bool {
	return q.used == q.capacity
}

func main() {
	q := New(5)

	q.Enqueue(1)
	q.Enqueue(2)
	q.Enqueue(3)
	q.Enqueue(4)
	fmt.Println(q.Front())
	fmt.Println(q.Rear())
	q.Dequeue()
	fmt.Println(q.Front())

}
