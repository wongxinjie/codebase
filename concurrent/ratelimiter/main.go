package main

import (
	"log"
	"time"
)

const (
	period    = time.Second
	rateLimit = 2
)

type Request interface{}

func handle(r Request) {
	log.Println(r)
}

func process(requests <-chan Request) {
	bucket := make(chan time.Time, rateLimit)

	// 每隔一段时间放进一个token
	go func() {
		tick := time.NewTicker(period / rateLimit)
		defer tick.Stop()

		for t := range tick.C {
			select {
			case bucket <- t:
			default:
			}
		}
	}()

	// 处理之前先拿token
	for r := range requests {
		<-bucket
		go handle(r)
	}
}

func main() {
	requests := make(chan Request)
	go process(requests)

	for i := 0; ; i++ {
		requests <- i
	}

}
