package main

import (
	"context"
	"log"
	"net"
	"strings"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/wongxinjie/codebase/rpc/pubsub/core"
	pubsub "gitlab.com/wongxinjie/codebase/rpc/pubsub/proto"
)

type PubsubService struct {
	pub *core.Publisher
	pubsub.UnimplementedPubsubServiceServer
}

func NewPubsubService() *PubsubService {
	return &PubsubService{pub: core.NewPublisher(100*time.Millisecond, 10)}
}

func (p *PubsubService) Publish(ctx context.Context, args *pubsub.Byte) (*pubsub.Byte, error) {
	p.pub.Publish(args.GetValue())
	log.Println("args ", args)
	return &pubsub.Byte{}, nil
}

func (p *PubsubService) Subscribe(args *pubsub.Byte, steam pubsub.PubsubService_SubscribeServer) error {
	ch := p.pub.SubscribeTopic(func(v interface{}) bool {
		if key, ok := v.(string); ok {
			if strings.HasPrefix(key, args.GetValue()) {
				return true
			}
		}
		return false
	})

	for v := range ch {
		if err := steam.Send(&pubsub.Byte{Value: v.(string)}); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	server := grpc.NewServer()
	pubsub.RegisterPubsubServiceServer(server, NewPubsubService())

	listener, err := net.Listen("tcp", ":9095")
	if err != nil {
		log.Fatal(err)
	}

	if err := server.Serve(listener); err != nil {
		log.Fatal(err)
	}
}
