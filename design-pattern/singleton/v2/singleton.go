package v2

import (
	"fmt"
	"sync"
)

type singleton struct {}

func (s *singleton) Call() {
	fmt.Printf("Addr: %p\n", s)
}

var (
	instance *singleton
	once sync.Once
)

func Instance() *singleton {
	once.Do(func() {
		instance = &singleton{}
	})
	return instance
}
