package main

import (
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitlab.com/wongxinjie/codebase/rpc/srpc/v1/api"
)

type HelloServiceClient struct {
	*rpc.Client
}

var _ api.HelloServiceInterface = (*HelloServiceClient)(nil)

func (c *HelloServiceClient) Hello(request string, reply *string) error {
	return c.Client.Call(api.HelloServiceName+".Hello", request, reply)
}

func DialHelloService(network, address string) (*HelloServiceClient, error) {
	conn, err := net.Dial(network, address)
	if err != nil {
		return nil, err
	}
	client := rpc.NewClientWithCodec(jsonrpc.NewClientCodec(conn))

	return &HelloServiceClient{Client: client}, nil
}

func main() {
	client, err := DialHelloService("tcp", "localhost:9092")
	if err != nil {
		log.Fatal(err)
	}

	var reply string
	err = client.Hello("today ", &reply)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("reply ", reply)
}
