package main

import (
	"fmt"
	"log"
	"time"
	"unsafe"
)

func longRunning(messages <-chan string) {
	timer := time.NewTimer(time.Second)
	defer timer.Stop()

	for {
		select {
		case <-timer.C:
			log.Println("超时")
			return
		case msg := <-messages:
			{
				log.Println("接收到消息# ", msg)
				if !timer.Stop() {
					<-timer.C
				}
			}
		}
		// 重置复用
		timer.Reset(time.Second)
	}
}

func main() {
	var p *struct{} = nil
	fmt.Println(unsafe.Sizeof(p))
}
