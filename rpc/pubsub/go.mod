module gitlab.com/wongxinjie/codebase/rpc/pubsub

go 1.16

require (
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
