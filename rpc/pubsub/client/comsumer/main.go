package main

import (
	"context"
	"log"

	"google.golang.org/grpc"

	pubsub "gitlab.com/wongxinjie/codebase/rpc/pubsub/proto"
)

func main() {
	conn, err := grpc.Dial("localhost:9095", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := pubsub.NewPubsubServiceClient(conn)

	_, err = client.Publish(
		context.Background(), &pubsub.Byte{Value: "golang: hello Go"},
	)
	if err != nil {
		log.Fatal(err)
	}
	_, err = client.Publish(
		context.Background(), &pubsub.Byte{Value: "docker: hello Docker"},
	)
	if err != nil {
		log.Fatal(err)
	}
}
