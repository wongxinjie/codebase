package wpool

import (
	"context"
)

type JobID string
type JobType string
type jobMetadata map[string]interface{}

type Fn func(ctx context.Context, args interface{}) (interface{}, error)

type JobDescriptor struct {
	ID       JobID
	Type     JobType
	Metadata map[string]interface{}
}

type Result struct {
	Value      interface{}
	Err        error
	Descriptor JobDescriptor
}

type Job struct {
	Descriptor JobDescriptor
	ExecFn     Fn
	Args       interface{}
}

func (job Job) execute(ctx context.Context) Result {
	rv, err := job.ExecFn(ctx, job.Args)
	if err != nil {
		return Result{
			Err:        err,
			Descriptor: job.Descriptor,
		}
	}

	return Result{Value: rv, Descriptor: job.Descriptor}
}
