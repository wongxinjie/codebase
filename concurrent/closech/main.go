package main

import "fmt"

type T struct{}

func worker(id int, ready <-chan T, done chan<- T) {
	<-ready
	fmt.Println("worker # ", id, " working")
	fmt.Println("worker # ", id, " done")
	done <- T{}
}

func main() {
	ready, done := make(chan T), make(chan T)
	go worker(1, ready, done)
	go worker(2, ready, done)
	go worker(3, ready, done)

	close(ready)
	<-done
	<-done
	<-done
}
