package robinrb

import (
	"fmt"
	"testing"

	"gitlab.com/wongxinjie/codebase/loadbalance"
)

func TestRobinLoadBalance_Select(t *testing.T) {
	rb := New()

	for i := 1; i <= 10; i++ {
		s := &loadbalance.Server{
			IP:     fmt.Sprintf("192.168.1.%d", i),
			Name:   fmt.Sprintf("server@%d", i),
		}
		rb.AddServer(s)
	}

	ip := "12.76.56.34"
	for i := 0; i < 15; i++ {
		t.Log(rb.Select(ip))
	}
}
