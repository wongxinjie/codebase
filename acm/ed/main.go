package main

import (
	"context"
	"encoding/json"
	"log"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

const (
	config = "/local/demo"
)

type Demo struct {
	Host string `json:"host"`
	Port int    `json:"port"`
}

func fetchClient() (*clientv3.Client, error) {
	return clientv3.New(clientv3.Config{
		Endpoints:   []string{"localhost:2379"},
		DialTimeout: 5 * time.Second,
	})
}

func main() {
	ctx := context.Background()

	client, err := fetchClient()
	if err != nil {
		log.Fatal(err)
	}

	demo := &Demo{
		Host: "localhost",
		Port: 6379,
	}
	bytes, err := json.Marshal(demo)
	if err != nil {
		log.Fatal(err)
	}

	putResp, err := client.Put(ctx, config, string(bytes))
	if err != nil {
		log.Fatal(err)
	}
	log.Println(putResp)

	resp, err := client.Get(ctx, config)
	if err != nil {
		log.Fatal(err)
	}
	for _, kv := range resp.Kvs {
		log.Println("key ", string(kv.Key))

		var c Demo
		if err := json.Unmarshal(kv.Value, &c); err != nil {
			log.Fatal(err)
		}
		log.Println("demo ", c)
	}

}
