package main

import (
	"fmt"
	"log"

	redisbloom "github.com/RedisBloom/redisbloom-go"
	"github.com/gomodule/redigo/redis"
)

type BloomFilter struct {
	client *redisbloom.Client
}

func NewBloomFilter(addr string) *BloomFilter {
	pool := &redis.Pool{
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr)
		},
	}
	client := redisbloom.NewClientFromPool(pool, "demo")
	return &BloomFilter{client: client}
}

func (b *BloomFilter) Add(key string, value int64) error {
	_, err := b.client.Add(key, fmt.Sprintf("%d", value))
	if err != nil {
		return err
	}
	return nil
}

func (b *BloomFilter) Exists(key string, value int64) (bool, error) {
	return b.client.Exists(key, fmt.Sprintf("%d", value))
}

func main() {

	bloomFilter := NewBloomFilter("localhost:6379")

	if ok, err := bloomFilter.Exists("falcon:u", 9999); err != nil {
		log.Fatal(err)
	} else {
		log.Println("user ", 9999, " exists ", ok)
	}

	for i := 0; i < 10000; i++ {
		userId := int64(i)
		fmt.Println(i)
		if err := bloomFilter.Add("falcon:u", userId); err != nil {
			log.Fatal(err)
		}
	}

	if ok, err := bloomFilter.Exists("falcon:u", 19999); err != nil {
		log.Fatal(err)
	} else {
		log.Println("user ", 9999, " exists ", ok)
	}

}
