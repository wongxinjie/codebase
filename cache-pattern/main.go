package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		time.Sleep(3 * time.Second)
		wg.Done()
	}()

	go func() {
		fmt.Println("wait 1")
		wg.Wait()
		fmt.Println("ok 1")
	}()

	go func() {
		fmt.Println("wait 2")
		wg.Wait()
		fmt.Println("ok 2")
	}()

	time.Sleep(1 * time.Second)
	fmt.Println("main wait")
	wg.Wait()
	fmt.Println("main ok")

}
