//+build wireinject

package pkg

import "github.com/google/wire"

func GetPostService() (*PostService, error){
	panic(wire.Build(
		NewPostService,
		NewPostUseCase,
		NewPostRepo))
}

