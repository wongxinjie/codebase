package main

import (
	"fmt"
	"runtime"
	"time"
)

func test() {
	type M [1 << 10]byte

	data := make([]*M, 1024*40)

	for i := 0; i < len(data); i++ {
		data[i] = new(M)
	}

	for i := 0; i < len(data); i++ {
		data[i] = nil
	}
}

func main() {
	test()

	for {
		var ms runtime.MemStats
		runtime.ReadMemStats(&ms)
		fmt.Printf("%s %d MB", time.Now().Format("15:04:05"), ms.NextGC>>20)
		time.Sleep(20 * time.Second)

	}
}
