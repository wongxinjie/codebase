module cachepattern

go 1.16

require (
	github.com/RedisBloom/redisbloom-go v1.0.0
	github.com/gomodule/redigo v1.8.2
)
