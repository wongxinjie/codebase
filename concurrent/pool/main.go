package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var bufferCount int32

func createBuffer() interface{} {
	atomic.AddInt32(&bufferCount, 1)
	buffer := make([]byte, 1024)
	return buffer
}

func main() {
	pool := &sync.Pool{New: createBuffer}

	worker := 1024 * 1024
	var wg sync.WaitGroup
	wg.Add(worker)

	for i := 0; i < worker; i++ {
		go func() {
			defer wg.Done()

			buffer := pool.Get()
			_ = buffer.([]byte)
			defer pool.Put(buffer)
		}()
	}
	wg.Wait()
	fmt.Printf("%d buffer objects created", bufferCount)
}
