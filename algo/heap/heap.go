package main

import "fmt"

type Heap struct {
	items []int
	size  int
}

func New() *Heap {
	return &Heap{
		items: make([]int, 0),
		size:  0,
	}
}

func (h *Heap) sink(i int) {
	var (
		j      int
		target = h.items[i]
	)

	for i*2+1 < h.size {
		j = i*2 + 1

		if j < h.size-1 && h.items[j] < h.items[j+1] {
			j++
		}

		if h.items[j] > target {
			h.items[i] = h.items[j]
			i = j
		} else {
			break
		}
	}
	h.items[i] = target
}

func (h *Heap) swim(i int) {
	var (
		target = h.items[i]
		parent = 0
	)

	for i > 0 && (i-1)/2 != i {
		parent = (i - 1) / 2
		if h.items[parent] < target {
			h.items[i] = h.items[parent]
			i = parent
		} else {
			break
		}
	}
	h.items[i] = target
}

func (h *Heap) Push(v int) {
	h.items = append(h.items, v)
	h.size++
	h.swim(h.size - 1)
}

func (h *Heap) Pop() int {
	rv := h.items[0]
	h.items[0] = h.items[h.size-1]
	h.items = h.items[:h.size-1]
	h.size--
	h.sink(0)
	return rv
}

func main() {
	nums := []int{5, 3, 6, 7, 6, 9, 1}

	heap := New()
	for _, n := range nums {
		heap.Push(n)
	}

	fmt.Println(heap.Pop())
	fmt.Println(heap.Pop())
}
