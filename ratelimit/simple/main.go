package main

import (
	"context"
	"log"
	"os"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

type Api struct {
	limiter *rate.Limiter
}

func Open(count int, duration time.Duration) *Api {
	return &Api{
		limiter: rate.NewLimiter(Per(count, duration), count),
	}
}

func (a *Api) ReadFd(ctx context.Context) error {
	if err := a.limiter.Wait(ctx); err != nil {
		return err
	}
	return nil
}

func (a *Api) ResolveAddr(ctx context.Context) error {
	if err := a.limiter.Wait(ctx); err != nil {
		return err
	}
	return nil
}

func Per(eventCount int, duration time.Duration) rate.Limit {
	return rate.Every(duration / time.Duration(eventCount))
}


func main() {
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Ltime | log.LUTC)

	api := Open(1, 5 *time.Second)


	var wg sync.WaitGroup
	wg.Add(20)

	ctx := context.Background()

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			if err := api.ReadFd(ctx); err != nil {
				log.Println("api ReadFd error", err)
			}
			log.Println("ReadFd")
		}()
	}

	for i := 0; i  < 10; i++ {
		go func() {
			defer wg.Done()
			if err := api.ResolveAddr(ctx); err != nil {
				log.Println("api ResolverAddr error", err)
			}
			log.Println("ResolveAddr")
		}()
	}

	wg.Wait()

}