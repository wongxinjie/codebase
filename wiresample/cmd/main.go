package main

import "gitlab.com/wongxinjie/codebase/sample/pkg"

func main() {
	svc, err := pkg.GetPostService()
	if err != nil {
		return
	}

	svc.Run()
}
