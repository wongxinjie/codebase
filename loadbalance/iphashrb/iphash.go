package iphashrb

import (
	"hash/fnv"

	"gitlab.com/wongxinjie/codebase/loadbalance"
)

const (
	localAddr = "127.0.0.1"
)

type IPHashLoadBalance struct {
	*loadbalance.Cluster
}

func New() *IPHashLoadBalance {
	return &IPHashLoadBalance{Cluster: loadbalance.NewCluster()}
}


func (ipr *IPHashLoadBalance) Select(ip string ) *loadbalance.Server {
	if ipr.EmptyOrOneServer() {
		return ipr.PickOne()
	}

	return ipr.doSelect(ip)
}

func (ipr *IPHashLoadBalance) doSelect(ip string) *loadbalance.Server {
	if len(ip) == 0 {
		ip = localAddr
	}

	idx := ipr.str2hash(ip) % len(ipr.Servers)
	return ipr.Servers[idx]
}

func (ipr *IPHashLoadBalance) str2hash(s string) int {
	h := fnv.New32()
	h.Write([]byte(s))
	n := h.Sum32()
	return int(n)
}