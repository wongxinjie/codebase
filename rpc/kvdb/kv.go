package kvdb

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	KVStoreServiceName = "KVStoreService"
)

type KVStoreService struct {
	dict   map[string]string
	filter map[string]func(key string)
	m      sync.Mutex
}

func NewKVStoreService() *KVStoreService {
	return &KVStoreService{
		dict:   make(map[string]string),
		filter: make(map[string]func(key string)),
	}
}

func (kv *KVStoreService) Get(key string, value *string) error {
	kv.m.Lock()
	defer kv.m.Unlock()

	if v, ok := kv.dict[key]; ok {
		*value = v
		return nil
	}
	return fmt.Errorf("key=%s not found", key)
}

func (kv *KVStoreService) Set(pair [2]string, reply *struct{}) error {
	kv.m.Lock()
	defer kv.m.Unlock()

	key, value := pair[0], pair[1]
	if ov := kv.dict[key]; ov != value {
		for _, fn := range kv.filter {
			fn(key)
		}
	}
	kv.dict[key] = value
	return nil
}

func (kv *KVStoreService) Watch(timeoutSecond int, keyChanged *string) error {
	id := fmt.Sprintf("watch-%s-%03d", time.Now(), rand.Int())
	ch := make(chan string, 10)

	kv.m.Lock()
	kv.filter[id] = func(key string) {
		ch <- key
	}
	kv.m.Unlock()

	select {
	case <-time.After(time.Duration(timeoutSecond) * time.Second):
		return fmt.Errorf("timeout")
	case key := <-ch:
		*keyChanged = key
	}
	return nil
}
