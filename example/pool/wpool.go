package main

import (
	"context"
	"fmt"

	"gitlab.com/wongxinjie/codebase/wpool"
)

func main() {
	workerPool := wpool.New(5)

	jobs := make([]wpool.Job, 0)
	for i := 0; i < 10; i++ {
		c := i
		job := wpool.Job{
			ExecFn: func(ctx context.Context, args interface{}) (interface{}, error) {
				fmt.Printf("args %+v\n", c)
				return nil, nil
			},
			Args: c,
		}
		jobs = append(jobs, job)
	}

	go workerPool.GenerateFrom(jobs)
	go workerPool.Run(context.Background())

	for {
		select {
		case rv, ok := <-workerPool.Results():
			if !ok {
				continue
			}
			fmt.Println("rv ", rv)
		case <-workerPool.Done:
			return
		}
	}
}
