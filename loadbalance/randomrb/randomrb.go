package randomrb

import (
	"math/rand"
	"time"

	"gitlab.com/wongxinjie/codebase/loadbalance"
)

type RandomLoadBalance struct {
	 *loadbalance.Cluster
}

func New() *RandomLoadBalance {
	return &RandomLoadBalance{
		Cluster: loadbalance.NewCluster(),
	}
}

func (rb *RandomLoadBalance) Select(ip string) *loadbalance.Server {
	if rb.EmptyOrOneServer() {
		return rb.PickOne()
	}

	return rb.doSelect(ip)
}

func (rb *RandomLoadBalance) doSelect(ip string) *loadbalance.Server {
	rand.Seed(time.Now().UnixNano())

	idx :=  rand.Intn(len(rb.Servers)) % len(rb.Servers)
	return rb.Servers[idx]
}
