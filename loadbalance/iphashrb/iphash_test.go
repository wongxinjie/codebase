package iphashrb

import (
	"fmt"
	"testing"

	"gitlab.com/wongxinjie/codebase/loadbalance"
)

func TestIPHashLoadBalance_str2hash(t *testing.T) {
	rb := New()

	fmt.Println(rb.str2hash("127.0.0.1"))
}

func TestIPHashLoadBalance_Select(t *testing.T) {
	rb := New()

	for i := 1; i <= 10; i++ {
		s := &loadbalance.Server{
			IP:     fmt.Sprintf("192.168.1.%d", i),
			Name:   fmt.Sprintf("server@%d", i),
		}
		rb.AddServer(s)
	}

	ip := "12.76.56.34"
	for i := 0; i < 5; i++ {
		t.Log(rb.Select(ip))
	}

	ip = "12.76.4.34"
	t.Log(ip)
	for i := 0; i < 5; i++ {
		t.Log(rb.Select(ip))
	}
}