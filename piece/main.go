package main

import (
	"fmt"
	"log"
	"reflect"
	"strconv"
	"sync"
	"time"
)

type Attach struct {
	BirthYear int    `json:"birth_year"`
	City      string `json:"city"`
}

type User struct {
	Attach
	Phone    string  `json:"phone" form:"mobile"`
	Password string  `json:"password" form:"pw"`
	Level    float64 `json:"level"`
}

func (u User) Login(s string) error {
	fmt.Println(u.Phone, s)
	return nil
}

func r() {
	u := User{
		Attach: Attach{
			BirthYear: 1997,
			City:      "GZ",
		},
		Phone:    "188",
		Password: "123456",
		Level:    9.0,
	}
	v := reflect.ValueOf(u)
	for k := 0; k < v.NumField(); k++ {
		vl := v.Field(k)
		switch vl.Type() {
		case reflect.TypeOf((float64)(0)):
			fmt.Println("float64 ", vl)
		case reflect.TypeOf(""):
			fmt.Println("string ", vl)
		default:
			fmt.Println("struct", vl)
		}
	}
}

type Book struct {
	pages int
}

func (b Book) Pages() int {
	return b.pages
}

func (b *Book) SetPages(pages int) {
	b.pages = pages
}

type I interface {
	m()
}

type T struct {
	I
}

func parseInt(s string) (int, error) {
	n, err := strconv.Atoi(s)
	if err != nil {
		b, err := strconv.ParseBool(s)
		if err != nil {
			return 0, err
		}
		if b {
			n = 1
		}
	}
	return n, err
}

func gen() <-chan int {
	c := make(chan int)
	go func() {
		var n int
		for {
			c <- n
			n++
			log.Println("n ", n)
			time.Sleep(time.Second)
		}
	}()
	return c
}

func escape() *int {
	t := 3
	return &t
}

func f(s []int) {
	for i := 0; i < len(s); i++ {
		s[i] = s[i] * 2
	}
}

type user struct {
	name string
	age  int
}

func fib(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}

}

func fn(m *sync.RWMutex, id int) {
	m.RLock()
	log.Println("id ", id, " ", time.Now())
	time.Sleep(50 * time.Millisecond)
	m.RUnlock()
}

type Duck interface {
	Quack()
}

type Cat struct {
}

func (c *Cat) Quack() {
	fmt.Println("cat ")
}

func recv(c chan int, wg *sync.WaitGroup) {
	fmt.Println(<-c)
	wg.Done()
}

func send(c chan int, wg *sync.WaitGroup) {
	c <- 10
}

func main() {
	var ch chan int

	var wg sync.WaitGroup
	wg.Add(2)
	go send(ch, &wg)
	go recv(ch, &wg)

	fmt.Println("blocking ")
	wg.Wait()

}
