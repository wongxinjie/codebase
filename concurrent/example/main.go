package main

import (
	"log"
	"math/rand"
	"time"
)

type Customer struct {
	id int
}
type Bar chan Customer

func (b Bar) ServeCustomer(c Customer) {
	log.Println("++顾客#", c.id, "开始饮酒")
	time.Sleep(time.Second * time.Duration(3+rand.Intn(15)))
	log.Println("--顾客#", c.id, "离开酒吧")
	<-b
}

func main() {
	rand.Seed(time.Now().UnixNano())

	bar := make(Bar, 10)
	for n := 0; ; n++ {
		time.Sleep(time.Second)
		c := Customer{id: n}
		select {
		case bar <- c:
			go bar.ServeCustomer(c)
		default:
			log.Println("顾客#", n, "不愿等待离去")
		}
	}

	select {}
}
