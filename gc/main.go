package main

import (
	"log"
	"runtime"
	"runtime/debug"
	"time"
)

func logGCStats() {
	ticker := time.NewTicker(time.Second)
	s := debug.GCStats{}

	for {
		select {
		case <-ticker.C:
			debug.ReadGCStats(&s)
			log.Printf("gc %d last @%v, pause_total %v\n", s.NumGC, s.LastGC, s.PauseTotal)
		}
	}
}

func logMemStats() {
	ticker := time.NewTicker(time.Second)
	s := runtime.MemStats{}
	for {
		select {
		case <-ticker.C:
			runtime.ReadMemStats(&s)
			log.Printf("gc %d last@%v, next_heap_size %vMB\n", s.NumGC, time.Unix(int64(time.Duration(s.LastGC).Seconds()), 0), s.NextGC/(1<<20))
		}
	}
}

func main() {
	go logMemStats()
	go logGCStats()

	time.Sleep(time.Minute)
}
