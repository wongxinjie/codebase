package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

// 使用 buffer channel 和 waitGroup 实现控制 goroutine 数量
type GoPool struct {
	queue chan struct{}
	wg    *sync.WaitGroup
}

func NewGoPool(size int) *GoPool {
	if size <= 0 {
		size = 1
	}
	return &GoPool{
		queue: make(chan struct{}, size),
		wg:    new(sync.WaitGroup),
	}
}

func (p *GoPool) Acquire(n int) {
	if n > 0 {
		for i := 0; i < n; i++ {
			p.queue <- struct{}{}
		}
	} else {
		for i := 0; i > n; i++ {
			<-p.queue
		}
	}
	p.wg.Add(n)
}

func (p *GoPool) Release() {
	<-p.queue
	p.wg.Done()
}

func (p *GoPool) Wait() {
	p.wg.Wait()
}

func main() {
	pool := NewGoPool(5)
	for i := 0; i < 20; i++ {
		pool.Acquire(1)
		go func(n int) {
			time.Sleep(time.Second)
			fmt.Println("GoroutineCount=", runtime.NumGoroutine())
			pool.Release()
		}(i)
	}

	pool.Wait()
	fmt.Println("GoroutineCount=", runtime.NumGoroutine())

}
