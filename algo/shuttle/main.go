package main

import (
	"fmt"
	"math/rand"
)

func Shuttle(nums []int) {
	for i := len(nums); i > 0; i-- {
		lastIdx := len(nums) - 1
		idx := rand.Int31n(int32(i))
		nums[idx], nums[lastIdx] = nums[lastIdx], nums[idx]
	}
}

func main() {
	nums := make([]int, 0)
	for i := 0; i < 100; i++ {
		nums = append(nums, i+1)
	}
	Shuttle(nums)
	fmt.Println(nums)
}
