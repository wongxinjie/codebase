package uniqueid

import (
	"sync"
	"testing"
	"time"
)

func TestFlake_ID(t *testing.T) {
	flake := NewFlake()

	count := 10
	var wg sync.WaitGroup
	wg.Add(count)

	for i := 0; i < count; i++ {
		go func() {
			id, err := flake.ID()
			if err != nil {
				t.Fatal(err)
			}
			t.Log(time.Now().Unix(), id, id.Int64(), id.String())
			wg.Done()
		}()
	}

	wg.Wait()
}
