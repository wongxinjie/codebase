package pkg

import "fmt"

type PostRepo interface {}

func NewPostRepo() PostRepo {
	return new(PostRepo)
}


type PostUseCaseAble interface {}
type postUseCase struct {
	repo PostRepo
}
func NewPostUseCase(repo PostRepo) PostUseCaseAble {
	return postUseCase{repo: repo}
}


type PostService struct {
	useCase PostUseCaseAble
}
func NewPostService(uc PostUseCaseAble) (*PostService, error) {
	return &PostService{useCase: uc}, nil
}

func (s *PostService) Run() {
	fmt.Println("server starting...")
}


