package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/ip", func(w http.ResponseWriter, r *http.Request) {
		ip, err := IpAddress()
		if err != nil {

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "your ip: %s", ip)
	})

	http.ListenAndServe("127.0.0.1:8080", nil)

}

// githubStatus retrieves GitHub's API status
func IpAddress() (string, error) {
	// Log the start and end of the function so we can see how many times it's called.
	log.Println("Making request to GitHub API")
	defer log.Println("Request to GitHub API Complete") // The defer causes this to be logged after the function's return statement.

	// Atrificially delay this function to emulate a long running task
	time.Sleep(1 * time.Second)

	// Make a request to the GitHub Status API
	resp, err := http.Get("http://api.ipify.org/")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	// Check that we got a good response.
	if resp.StatusCode != 200 {
		return "", fmt.Errorf("ip response: %s", resp.Status)
	}

	// Decode the JSON response
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	content := string(bytes)

	return content, nil
}