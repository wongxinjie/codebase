package wpool

import (
	"context"
	"sync"
)

type WorkerPool struct {
	workerCount int
	jobs        chan Job
	results     chan Result
	Done        chan struct{}
}

func New(count int) *WorkerPool {
	return &WorkerPool{
		workerCount: count,
		jobs:        make(chan Job, count),
		results:     make(chan Result, count),
		Done:        make(chan struct{}),
	}
}

func (w WorkerPool) Run(ctx context.Context) {
	var wg sync.WaitGroup
	for i := 0; i < w.workerCount; i++ {
		wg.Add(1)
		go process(ctx, &wg, w.jobs, w.results)
	}

	wg.Wait()
	close(w.Done)
	close(w.results)
}

func (w WorkerPool) Results() <-chan Result {
	return w.results
}

func (w WorkerPool) GenerateFrom(jobs []Job) {
	for i := 0; i < len(jobs); i++ {
		w.jobs <- jobs[i]
	}
	close(w.jobs)
}

func process(ctx context.Context, wg *sync.WaitGroup, jobs <-chan Job, results chan<- Result) {
	defer wg.Done()

	for {
		select {
		case job, ok := <-jobs:
			if !ok {
				return
			}
			results <- job.execute(ctx)
		case <-ctx.Done():
			results <- Result{Err: ctx.Err()}
			return
		}
	}
}
