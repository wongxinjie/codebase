package main

import (
	"fmt"
	"sync"
	"time"
)

type ConcurrentMap struct {
	data map[interface{}]interface{}
	m    sync.RWMutex
}

func NewConcurrentMap() *ConcurrentMap {
	return &ConcurrentMap{data: make(map[interface{}]interface{})}
}

func (cm *ConcurrentMap) Load(k interface{}) (interface{}, bool) {
	cm.m.RLock()
	defer cm.m.RUnlock()
	v, ok := cm.data[k]
	return v, ok
}

func (cm *ConcurrentMap) Store(k, v interface{}) {
	cm.m.Lock()
	defer cm.m.Unlock()
	cm.data[k] = v
}

func fn(k, v int, m *ConcurrentMap) {
	m.Store(k, v)
}

func main() {
	m := NewConcurrentMap()
	for i := 0; i < 10; i++ {
		go fn(i, i+100, m)
	}

	fmt.Println(m)
	time.Sleep(time.Second)
}
