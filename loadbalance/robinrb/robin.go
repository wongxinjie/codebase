package robinrb

import (
	"sync/atomic"

	"gitlab.com/wongxinjie/codebase/loadbalance"
)

type RobinLoadBalance struct {
	position int32
	*loadbalance.Cluster
}

func New() *RobinLoadBalance {
	return &RobinLoadBalance{Cluster: loadbalance.NewCluster()}
}

func (rb *RobinLoadBalance) Select(ip string) *loadbalance.Server {
	if rb.EmptyOrOneServer() {
		return rb.PickOne()
	}

	return rb.doSelect(ip)
}

func (rb *RobinLoadBalance) doSelect(ip string) *loadbalance.Server {
	size := len(rb.Servers)

	atomic.CompareAndSwapInt32(&rb.position, int32(size), 0)
	server := rb.Servers[rb.position]
	atomic.AddInt32(&rb.position, 1)
	return server
}
