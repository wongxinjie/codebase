package uniqueid

import (
	"strconv"
	"time"

	"github.com/sony/sonyflake"
)

var (
	defaultStartTime = time.Date(2015, 7, 1, 0, 0, 0, 0, time.UTC)
)

func getMachineID() (uint16, error) {
	// 这里可以使用Redis来记录machineID
	return 12, nil
}

func checkMachineID(machineID uint16) bool {
	// 可以使用redis来记录
	return true
}

type ID struct {
	id uint64
}

func (t *ID) Int64() int64 {
	return int64(t.id)
}

func (t *ID) String() string {
	return strconv.FormatInt(int64(t.id), 10)
}

type Flake struct {
	sf *sonyflake.Sonyflake
}

func NewFlake() *Flake {
	settings := sonyflake.Settings{
		StartTime:      defaultStartTime,
		MachineID:      getMachineID,
		CheckMachineID: checkMachineID,
	}
	sf := sonyflake.NewSonyflake(settings)

	if sf == nil {
		panic("NewFlake error")
	}
	return &Flake{sf: sf}
}

func (s *Flake) ID() (*ID, error) {
	nextID, err := s.sf.NextID()
	if err != nil {
		return nil, err
	}
	return &ID{id: nextID}, nil
}
