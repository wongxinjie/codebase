package main

import (
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitlab.com/wongxinjie/codebase/rpc/srpc/v1/api"
)

func RegisterHelloService(svc api.HelloServiceInterface) error {
	return rpc.RegisterName(api.HelloServiceName, svc)
}

type HellService struct {
}

func (s *HellService) Hello(request string, reply *string) error {
	*reply = request + "received, echo"
	return nil
}

func main() {
	if err := RegisterHelloService(new(HellService)); err != nil {
		log.Fatal(err)
	}

	listener, err := net.Listen("tcp", ":9092")
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go rpc.ServeCodec(jsonrpc.NewServerCodec(conn))
	}

}
